import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import com.csvreader.CsvReader;

public class ReadFileWriteToConsole {

	private static final String inFile = "in.csv";
	private static CsvReader csv;

	public static void main(String args[]) throws IOException {
		// (add code to implement the following)
		// create a ReadFileWriteToConsole object
		// call method below to read the data from inFile
		// call method below to write the data to the console
		// Do not change the signature of this method.a
		ReadFileWriteToConsole rw = new ReadFileWriteToConsole();
		rw.makeSheet();
		rw.writeSheet();
		return;
	}

	public String getCell(int row, int col) {
		// return the value of the spreadsheet at the given row and column
		// Do not change the signature of this method.
		if (row > this.getRowCount() || col > this.getColCount()) {
			return null;
		}

		try {
			this.makeSheet();
			for (int i = 0; i <= row; i++) {
				csv.readRecord();
			}
			return csv.get(col);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public int getRowCount() {
		// return the number of rows in the spreadsheet
		// Do not change the signature of this method. // <-- fix this to return a
		// proper value
		try {
			int row = 0;
			makeSheet();
			while (csv.readRecord()) {
				row++;
			}
			return row;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	public int getColCount() {
		// return the number of columns in row
		// Do not change the signature of this method.
		try {
			makeSheet();
			csv.readRecord();
			return csv.getColumnCount();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	public void makeSheet() throws IOException {
		// read the data from inFile
		// Do not change the signature of this method.
		csv = new CsvReader(inFile);
		return;
	}

	public void writeSheet() {
		// format data to console
		// Do not change the signature of this method.
		for (int i = 0; i < this.getRowCount(); i++) {
			for (int j = 0; j < this.getColCount(); j++) {
				System.out.print("[" + this.getCell(i, j) + "]");
			}
			System.out.println();
		}
		return;
	}
}
