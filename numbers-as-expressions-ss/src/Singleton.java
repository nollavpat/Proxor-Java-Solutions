class Singleton {
	private static Singleton uniqueI;
	
	private Singleton() {};
	
	public static Singleton getInstance() {
		if (uniqueI == null) {
			uniqueI = new Singleton();
		}
		return uniqueI;
	}
}