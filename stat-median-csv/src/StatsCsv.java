// StatsCsv.java -- add greatest values to spreadsheet
//
// This is NOT a working program. This is a "skeleton" that
// should be modified and extended to meet the specifications.

import java.io.IOException;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;


public class StatsCsv {
    
    private static final int MAXROW = 16;
    private static final int MAXCOL = 16;
    private int rowsUsed = 0;
    private int colsUsed = 0;
    private static final String inFile = "Data011.csv";
    private static final String outFile = "Data02.csv"; 
   
    public String[][] sheet = new String[MAXROW][MAXCOL];
    
    public static void main(String[] args) throws IOException {

        StatsCsv a = new StatsCsv();
        a.readSheet();
        // a.getRows() is the row count before adding medians
        Stats s = new Stats(a.getRows(), a.getCols(), a.getData());
        a.setRows(a.getRows() + 1); // because we added a row of medians
        a.writeSheet();
    }
    
    public void writeSheet() throws IOException {
        // 
        // to be completed
    	//  Do not change the signature of this method.
    	CsvWriter csv = new CsvWriter(outFile);
    	for (int i = 0; i < rowsUsed; i++) {
			for (int j = 0; j < colsUsed; j++) {
				csv.write(sheet[i][j]);
				System.out.print("[" + sheet[i][j] + "]");
			}
			csv.endRecord();
			System.out.println();
		}
    	csv.close();
    }

    public void readSheet( ) throws IOException {
        // 
        // to be completed
    	//  Do not change the signature of this method.
    	CsvReader csv = new CsvReader(inFile);
    	for (int i = 0; csv.readRecord() && i < MAXROW; i++) {
    		if (rowsUsed == 0) {
				colsUsed = csv.getColumnCount();
			}
    		for (int j = 0; j < colsUsed && j < MAXCOL; j++) {
				sheet[rowsUsed][j] = csv.get(j);
				
			}
			rowsUsed++;
		}
    	
    	csv.close();
    }

    public int getRows(){
    	return rowsUsed;
    }
    
    public int setRows(int r) {
        rowsUsed = r;
        return rowsUsed;
    }

    public int getCols() {
    	return colsUsed;
    }
    
    public String[][] getData() {
    	return sheet;
    }
}
