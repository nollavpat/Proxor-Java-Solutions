import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

class Stats {
	int rows;
	int cols;
	String[][] data;

	public Stats(int rows, int cols, String[][] data) {
		// TODO Auto-generated constructor stub
		this.rows = rows;
		this.cols = cols;
		this.data = data;
		computeMedians();
	}

	public void computeMedians() {
		data[rows][0] = "";
		data[rows][1] = "Median";

		for (int i = 2; i < cols; i++) {
			String[] currentCol = new String[rows - 1];
			for (int j = 1; j < rows; j++) {
				currentCol[j - 1] = data[j][i];
			}
			Arrays.sort(currentCol, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					// TODO Auto-generated method stub
					return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
				}
			});
			System.out.println(Arrays.toString(currentCol));
			if (currentCol.length % 2 == 1) {
				data[rows][i] = currentCol[(currentCol.length - 1) / 2];
			} else {
				int up = Integer.parseInt(currentCol[(int) Math.ceil((currentCol.length - 1) / 2.0)]);
				int down = Integer.parseInt(currentCol[(int) Math.floor((currentCol.length - 1) / 2.0)]);
				data[rows][i] = Integer.toString((int) Math.ceil((up + down) / 2.0));
			}
		}
	}
}