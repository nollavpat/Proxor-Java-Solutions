public class Stats {
	private int rows;
	private int cols;
	private String[][] data;

	Stats(int rows, int cols, String[][] data) {
		this.rows = rows;
		this.cols = cols;
		this.data = data;
		getGreatestValue();
	}

	public void getGreatestValue() {
		data[rows][0] = "Greatest";
		data[rows][1] = "Value";
		for (int i = 2; i < this.cols; i++) {
			String value = "0";
			for (int j = 1; j < this.rows; j++) {
				double currentValue = Integer.parseInt(data[j][i]);
				if (currentValue > Integer.parseInt(value)) {
					value = data[j][i];
				}
			}
			data[rows][i] = value;
		}
	}
}