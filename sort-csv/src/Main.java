
// Main.java -- sort-cvs task base code
//
// This is NOT a working program. This is a "skeleton" that
// should be modified and extended to meet the specifications.

import java.io.FileNotFoundException;
import java.io.IOException;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class Main {
	private static final int MAXROW = 16;
	private static final int MAXCOL = 16;
	private int rowsUsed = 0;
	private int colsUsed = 0;
	private static final String inFile = "input.csv";
	private static final String outFile = "output.csv";

	public String[][] grades = new String[MAXROW][MAXCOL];
	
	private CsvReader cr;
	
	public static void main(String[] args) throws IOException {
		Main a = new Main();
		a.readData();
		// The following lines are commented out because Sort is
		// undefined. You should define Sort and uncomment these lines:
		a.writeData();
	}

	public void writeData() throws IOException {
		//
		// to be completed
		// Do not change the signature of this method.
		Sort s = new Sort(getRows(), getCols(), getData());
		String[][] newdata = s.sort();
		CsvWriter cw = new CsvWriter(outFile);
    	for (int r = 0; r < getRows(); r++) {
			for (int c = 0; c < getCols(); c++) {
				cw.write(newdata[r][c]);
			}
			cw.endRecord();
		}
    	cw.close();
	}

	public void readData() throws IOException {
		CsvReader cr = new CsvReader(inFile);
    	for (int r = 0; cr.readRecord() && r < MAXROW; r++) {
			String[] vals = cr.getValues();
			rowsUsed++;
    		for (int c = 0; c < cr.getColumnCount() && c < MAXCOL; c++) {
    			grades[r][c] = vals[c];
				colsUsed = cr.getColumnCount();
			}
		}
    	cr.close();
	}

	public int getRows() {
		try {
			cr = new CsvReader(inFile);
			int rowsUsed = 0;
			while (cr.readRecord()) {
				rowsUsed++;
			}
			return rowsUsed; 

		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public int setRows(int r) {
		rowsUsed = r;
		return rowsUsed;
	}

	public int getCols() {
		return colsUsed;
	}

	public String[][] getData() {
		//String[][] arrGradesRanges = {{"Cdas das","Easd asd"},{"Aas dsad","Aasdsd"},{"Zs","Bs"},{"Fs","Dd"}};
		return grades;
	}
}
