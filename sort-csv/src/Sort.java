import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Sort extends Main {
	private int rows;
	private int cols;
	private String[][] data;

	public Sort(int rows, int cols, String[][] data) {
		this.rows = rows;
		this.cols = cols;
		this.data = data;
	}

	public String[][] sort() {
		
		
		String[][] gradesK = new String[rows][cols];

		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				[r][c] = data[r][c];
			}
		}
			
		Arrays.sort(gradesK, new Comparator<String[]>() {
			int col = 1;
			@Override
			public int compare(String[] o1, String[] o2) {
				// TODO Auto-generated method stub
				return o1[col].compareTo(o2[col]);
			}
		});
		for (String[] arrf1 : gradesK) {
            System.out.println(Arrays.toString(arrf1));
        }       
		
        return gradesK;
		
	}
	
}

//class ColumnArrayComparator implements Comparator<Comparable[]>
//{
//    private int column = 0;
// 
//    public ColumnArrayComparator(int column)
//    {
//        this.column = column;
//    }
// 
//    @Override
//    public int compare(Comparable[] a1, Comparable[] a2)
//    {
//        return a1[column].compareTo( a2[column] );
//    }
//};

