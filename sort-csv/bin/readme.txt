readme.txt -- for sort-csv

THE TASK
Read a data file, 
sort according to last name in ascending order, and
write sorted data to a second file.

In detail, the following subtasks must be completed:

1. Write the implementation of the readData() method of class
   Main. Reading from the file must be done with CsvReader. 
   The Main method is provided for you and it calls
   readData. (The data format is described below.) 

2. The readData() method must store the data in grades (an instance
   variable in Main).

3. Create a class named Sort in a separate file (Sort.java).
   The class Sort is used to sort the input file data.

4. Sort initialization: As implied by the provided code in
   Main.java, the Sort::Sort constructor takes three arguments:
   the number of rows used, the number of columns used, and a matrix
   of String values (grades).

5. Write a method, Sort::sort, that sorts the data according to
   last name, in ascending order.
   NOTE: Sort should not copy the matrix of String values passed
   to its constructor so that after sorting, the grades array will
   be sorted.

6. Write the implementation of the writeData() method of
   Main. Writing the output file must be done with CsvWriter. The
   main() method calls writeData. (The data format is described below.)

   See input.csv for the format of an input file. The output file must 
   have the same format as the input.  (Note that the input
   file name, input.csv, and the output file name, output.csv, are
   provided as constants---you should not change these names.

The input data are organized by rows of people with scores on
tasks. Last names (sort keys) are in the second column.
You can read these data as strings using default settings in the
javacsv library. You may assume that the input data are valid. You may
assume that MAXROW and MAXCOL are large enough to store all the data
that will be read by readData().

