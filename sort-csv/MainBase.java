
// Main.java -- sort-cvs task base code
//
// This is NOT a working program. This is a "skeleton" that
// should be modified and extended to meet the specifications.

import java.io.IOException;

public class MainBase {
	private static final int MAXROW = 16;
	private static final int MAXCOL = 16;
	private int rowsUsed = 0;
	private int colsUsed = 0;
	private static final String inFile = "input.csv";
	private static final String outFile = "output.csv";

	private String[][] grades = new String[MAXROW][MAXCOL];

	public static void main(String[] args) throws IOException {
		MainBase a = new MainBase();
		a.readData();
		// The following lines are commented out because Sort is
		// undefined. You should define Sort and uncomment these lines:
		// Sort s = new Sort(a.getRows(), a.getCols(), a.getData());
		// s.sort(); // sort the grades
		a.writeData();

	}

	public void writeData() throws IOException {
		//
		// to be completed
		// Do not change the signature of this method.
	}

	public void readData() throws IOException {
		//
		// to be completed
		// Do not change the signature of this method.
	}

	public int getRows() {
		return rowsUsed;
	}

	public int setRows(int r) {
		rowsUsed = r;
		return rowsUsed;
	}

	public int getCols() {
		return colsUsed;
	}

	public String[][] getData() {
		return grades;
	}
}
