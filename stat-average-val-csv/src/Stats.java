import java.util.Arrays;

public class Stats extends StatsCsv {
	private int rows;
	private int cols;
	private String[][] data;

	public Stats(int rows, int cols, String[][] data) {
		this.rows = rows;
		this.cols = cols;
		this.data = data;
		
	}

	public void average() {
		data[rows][0] = "Average";
		data[rows][1] = "Value";
		
		
		for (int c = 2; c < cols; c++) {
			double counter=0;
			int sum = 0;
			for (int r = 1; r < rows; r++) {
				int val = Integer.parseInt(data[r][c]);
				sum = sum + val;
				counter++;
			}
			double total = sum / counter;
			System.out.println("total:" + total + ", arts: " + Math.round(total * 100.0) / 100.0);
            double round = Math.round(total * 100.0) / 100.0;
			data[rows][c] =  round+"";		
		}
		
	}
}