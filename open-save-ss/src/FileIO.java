
// FileIOBase.java -- template for FileIO.java.
//
// this file should be modified to implement
// open, save, and save as... commands for SpreadSheet.java
// 
// Do not modify the signatures of these methods.

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class FileIO {
	public static boolean open(SpreadSheet ss, File file) {
		System.out.println("Open " + file);

		for (int i = 0; i < ss.maxRows; i++) { // clear ss
			for (int j = 0; j < ss.maxCols; j++) {
				ss.setCell(i, j, "");
			}
		}

		ss.evaluate();

		try {
			CsvReader csv = new CsvReader(file.toString());
			int row = 0;

			while (csv.readRecord()) {
				for (int i = 0; i < csv.getColumnCount(); i++) {
					ss.setCell(row, i, csv.get(i));
				}
				row++;
			}

			ss.evaluate();

			csv.close();

			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}

	public static boolean saveAs(SpreadSheet ss, File file) {
		System.out.println("SaveAs " + file);
		int rows = 0;
		int cols = 0;

		for (int i = 0; i < ss.maxRows; i++) {
			boolean isOccupied = false;
			for (int j = 0; j < ss.maxCols; j++) {
				if (!ss.getCellFormula(i, j).equals("")) {
					isOccupied = true;
					if (j > cols) {
						cols = j;
					}
				}
			}
			if (isOccupied) {
				if (i > rows) {
					rows = i;
				}
			}
		}

		System.out.println("row: " + rows + " ,cols: " + cols);

		CsvWriter csv;
		try {
			csv = new CsvWriter(new FileWriter(file), ',');
			for (int i = 0; i <= rows; i++) {
				for (int j = 0; j <= cols; j++) {
					csv.write(ss.getCellFormula(i, j));
				}
				csv.endRecord();
			}
			csv.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return false;
	}
}
