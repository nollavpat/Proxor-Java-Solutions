import java.io.FileNotFoundException;
import java.io.IOException;
import com.csvreader.CsvReader;

public class ReadFileWriteToConsole {

	private static final int MAXROW = 3;
	private static final int MAXCOL = 3;
	private static final String inFile = "in.csv";
	private static CsvReader csv;

	public String[][] sheet = new String[MAXROW][MAXCOL];

	public static void main(String args[]) throws IOException {
		// create a ReadWriteToConsole object
		// Do not change the signature of this method.
		// ... insert code here ...
		// invoke readSheet()
		// ... insert code here ...
		// invoke writeSheet()
		// ... insert code here ...
		ReadFileWriteToConsole rw = new ReadFileWriteToConsole();
		rw.readSheet();
		rw.writeSheet();
	}

	public void readSheet() throws IOException {
		// ... insert code here ...
		// Do not change the signature of this method.
		csv = new CsvReader(inFile);
		for (int i = 0; i < MAXROW; i++) {
			csv.readRecord();
			for (int j = 0; j < MAXCOL; j++) {
				sheet[i][j] = csv.get(j);
			}
		}
		csv.close();
	}

	public void writeSheet() {
		// ... insert code here ...
		// Do not change the signature of this method.
		for (int i = 0; i < MAXROW; i++) {
			for (int j = 0; j < MAXCOL; j++) {
				System.out.print("[" + sheet[i][j] + "]");
			}
			System.out.println();
		}
	}
}
